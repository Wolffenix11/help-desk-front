import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsTicketComponent } from './ui/details-ticket/details-ticket.component';
import { ListTicketsComponent } from './ui/list-tickets/list-tickets.component';
import { RegisterTicketComponent } from './ui/register-ticket/register-ticket.component';

const routes: Routes = [
    { path: 'list-tickets', component: ListTicketsComponent },
    { path: 'register-ticket', component: RegisterTicketComponent },
    { path: 'details-ticket/:ticketId', component: DetailsTicketComponent },
    { path: '', pathMatch: 'full', redirectTo: '/list-tickets'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
