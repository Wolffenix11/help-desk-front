import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, pipe } from "rxjs";
import { map } from "rxjs/operators"
import { Ticket, RegisterTicket } from "../model/tickets.model";
import { DefaultResponse } from "../model/response.model";

const URL_BASE = "http://localhost:8080/ticket/"
const URL_FIND_BY_NAME_ISSUER = URL_BASE + "byNameIssuer?nameIssuer="
const URL_FIND_BY_NAME_RESPONDER = URL_BASE + "byNameResponder?nameResponder="


@Injectable({
    providedIn: 'root'
})
export class TicketService {

    constructor(private http: HttpClient) {


    }
    findByNameIssuer(name_issuer: string): Observable<Ticket[]> {
        return this.http.get<DefaultResponse<Ticket[]>>(URL_FIND_BY_NAME_ISSUER + name_issuer).pipe(map(response => response.result))}

    findById(id: number): Observable<Ticket> {
        return this.http.get<DefaultResponse<Ticket>>(URL_BASE + "/" + id).pipe(map(response => response.result))}
    
    findByNameResponder(name_responder: string): Observable<Ticket[]> {
        return this.http.get<DefaultResponse<Ticket[]>>(URL_FIND_BY_NAME_RESPONDER + name_responder).pipe(map(response => response.result))
    }


    findAll(): Observable<Ticket[]> {
        return this.http.get<DefaultResponse<Ticket[]>>(URL_BASE).pipe(
            map(response => response.result)
        )
    }

    save(ticket: RegisterTicket): Observable<boolean> {
        return this.http.post<DefaultResponse<boolean>>(URL_BASE, ticket).pipe(
            map(response => response.result)
        )
    }

}