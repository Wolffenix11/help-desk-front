import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ticket } from 'src/app/model/tickets.model';
import { TicketService } from 'src/app/service/tickets.service';

@Component({
    selector: 'app-details-ticket',
    templateUrl: './details-ticket.component.html',
    styleUrls: ['./details-ticket.component.css']
})
export class DetailsTicketComponent implements OnInit {


    ticket: Ticket = new Ticket()

    constructor(private route: ActivatedRoute, 
                private service: TicketService) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            let TicketId = params['ticketId']
            this.findTicket(TicketId)
        })        
    }

    findTicket(ticketId: number) {
        this.service.findById(ticketId).subscribe(
            result => {
                this.ticket = result
            }
        )
    }
}
