import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterTicket } from 'src/app/model/tickets.model';
import { TicketService } from 'src/app/service/tickets.service';

@Component({
    selector: 'app-register-ticket',
    templateUrl: './register-ticket.component.html',
    styleUrls: ['./register-ticket.component.css']
})
export class RegisterTicketComponent {

    formTicket = this.formBuilder.group({
        name_issuer: ['', [Validators.required, Validators.minLength(4)]],
        email_issuer: ['', [Validators.required, Validators.email, Validators.maxLength(255)]],
        name_responder: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255)]],
        email_responder: ['',  [Validators.required, Validators.email, Validators.maxLength(255)]],
        title: ['', [Validators.required, Validators.maxLength(255)]],
        description: ['', [Validators.required, Validators.maxLength(1024)]]
    })

    

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private service: TicketService) { }

    save() {
        let ticket = new RegisterTicket(
            this.name_issuer.value,
            this.email_issuer.value,
            this.name_responder.value,
            this.email_responder.value,
            this.title.value,
            this.description.value
        )      
        
        this.service.save(ticket).subscribe(
            result => this.cancel()
        )
    }

    cancel() {
        this.router.navigateByUrl("/")
    }

    get ticket_id() {
        return this.formTicket.controls.ticket_id
    }

    get name_issuer() {
        return this.formTicket.controls.name_issuer
    }

    get email_issuer() {
        return this.formTicket.controls.email_issuer
    }

    get name_responder() {
        return this.formTicket.controls.name_responder
    }

    get email_responder() {
        return this.formTicket.controls.email_responder
    }

    get title() {
        return this.formTicket.controls.title
    }

    get description() {
        return this.formTicket.controls.description
    }

}
