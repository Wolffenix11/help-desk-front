export class Ticket {
    public id: number
    public nameIssuer: string
    public emailIssuer: string
    public nameResponder: string
    public emailResponder: string
    public title: string
    public description: string
}

export class RegisterTicket {
    public nameIssuer: string
    public emailIssuer: string
    public nameResponder: string
    public emailResponder: string
    public title: string
    public description: string


    constructor(name_issuer: string,
                email_issuer: string,
                name_responder: string,
                email_responder: string,
                title: string,
                description: string) {
        
        this.nameIssuer        = name_issuer
        this.emailIssuer       = email_issuer
        this.nameResponder     = name_responder
        this.emailResponder    = email_responder
        this.title             = title
        this.description       = description
    }
}