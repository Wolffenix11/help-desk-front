import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';
import { ListTicketsComponent } from './ui/list-tickets/list-tickets.component';
import { RegisterTicketComponent } from './ui/register-ticket/register-ticket.component';
import { DetailsTicketComponent } from './ui/details-ticket/details-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    ListTicketsComponent,
    RegisterTicketComponent,
    DetailsTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
